﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidExample
{
    interface IUser
    {
        bool userloging(string userName,string passWord );
        bool userRegister(string userName, string passWord,string eMail);
    }
    interface IExceptionLogger
    {
        void logError(string error);
    }
    interface IEmail
    {
        void sendEmail(string emailContent);

    }
    interface IVerfyHuman
    {
        bool isHuman(List<User> roadSigns);
    }
}
